import styled from "styled-components";
import {NavLink} from "react-router-dom";

export const StyledLink = styled(NavLink)`
    text-decoration: none;
    color: #919191;
    &:focus, &:hover, &:visited {
        text-decoration: none;
        color: rgba(32,33,36,0.8);
    }
`;