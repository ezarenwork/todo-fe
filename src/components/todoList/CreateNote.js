import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";

import {createNote} from "../../store/todo/todo-actions";
import {Card} from "../common/StyledComponents/Card";
import {Button} from "../common/StyledComponents/Button";
import {TextArea} from "../common/StyledComponents/TextArea";
import Box from "@material-ui/core/Box";


export const CreateNote = () => {

	const dispatch = useDispatch();

	const [inputText, setInputText] = useState("");
	const [showCreateButton, setCreateButtonVisible] = useState(false);

	const handleCreateNote = () => {
		if (inputText && inputText.length > 0) {
			dispatch(createNote(inputText));
			setInputText("");
		}
	};

	const createNoteOrClearInput = (e) => {
		if (!e.shiftKey && e.key === 'Enter') {
			e.preventDefault();
			handleCreateNote();
		}
		if (e.key === 'Escape') {
			e.preventDefault();
			setInputText("");
		}
	};


	return (
		<Box
			mx={5} mb={1}
			onMouseEnter={() => setCreateButtonVisible(true)}
			onMouseLeave={() => setCreateButtonVisible(false)}
		>
			<Card>
				<Grid
					container
					justify="space-between"
					alignItems="center"
				>
					<Grid item sm>
						<TextArea
							fontSize='18px'
							placeholder="Note..."
							value={inputText}
							onKeyDown={createNoteOrClearInput}
							onChange={(e) => setInputText(e.currentTarget.value)}
						/>
					</Grid>
					<Grid item>
						<Button
							visibility={showCreateButton ? "visible" : "hidden"}
							onClick={handleCreateNote}>
							&#65291;
						</Button>
					</Grid>
				</Grid>
			</Card>
		</Box>
	)
};
