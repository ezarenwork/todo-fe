import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";

import {createSubNote} from "../../store/todo/todo-actions";
import {Button} from "../common/StyledComponents/Button";
import {TextArea} from "../common/StyledComponents/TextArea";
import Box from "@material-ui/core/Box";


const CreateSubNote = (props) => {

	const dispatch = useDispatch();

	const {noteId} = props;
	const [inputText, setInputText] = useState("");
	const [showCreateButton, setCreateButtonVisible] = useState(false);

	const handleCreateNote = () => {
		if (inputText && inputText.length > 0) {
			dispatch(createSubNote(noteId, inputText));
			setInputText("");
		}
	};

	const createNoteOrClearInput = (e) => {
		if (!e.shiftKey && e.key === 'Enter') {
			e.preventDefault();
			handleCreateNote();
		}
		if (e.key === 'Escape') {
			e.preventDefault();
			setInputText("");
		}
	};


	return (
		<Box ml={8} my={1}
		     onMouseEnter={() => setCreateButtonVisible(true)}
		     onMouseLeave={() => setCreateButtonVisible(false)}
		>
			<Grid
				container
				justify="space-between"
				alignItems="center"
			>
				<Grid item sm>
					<TextArea
						placeholder="add task..."
						value={inputText}
						onKeyDown={createNoteOrClearInput}
						onChange={(e) => setInputText(e.currentTarget.value)}
					/>
				</Grid>
				<Grid item>
					<Button
						visibility={showCreateButton ? "visible" : "hidden"}
						onClick={handleCreateNote}>
						&#65291;
					</Button>
				</Grid>
			</Grid>
		</Box>
	)
};

export default CreateSubNote;
