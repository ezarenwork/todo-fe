import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import {NavLink, useHistory} from "react-router-dom";
import {ErrorMessage, Form, Formik, useField} from "formik";
import {
	Grid,
	TextField as TextFieldMui,
	Button,
	Paper
} from "@material-ui/core";
import * as Yup from 'yup';

import {register} from "../../store/auth/auth-actions";
import Alert from "../common/Alert";
import {useAuth} from "../../store/auth/auth-selector";
import {AuthorizationManager} from "../../utils/authorization-manager";
import {useLocation} from "react-router";


const TextField = ({name, label, variant, type, ...props}) => {
	const [field, meta] = useField({name, ...props});
	const errorText = meta.error && meta.touched ? meta.error : '';
	return (
		<Fragment>
			<Grid item xs>
				<TextFieldMui
					style={{minWidth: 250}}
					variant={variant}
					placeholder={label}
					type={type}
					{...field}
					error={!!errorText}
				/>
			</Grid>
			<ErrorMessage name={name}>{msg => msg}</ErrorMessage>
		</Fragment>
	)
};

const initValues = {
	username: '',
	email: '',
	password: '',
	confirmPassword: ''
};

const signUpSchema = Yup.object().shape({
	username: Yup.string()
		.required()
		.min(3, 'Name is to short - should be 3 chars minimum')
		.max(20, 'Name is to long - should be 20 chars maximum'),
	email: Yup.string()
		.email("Not a valid email")
		.required(),
	password: Yup.string()
		.required('No password provided.')
		.min(6, 'Password is too short - should be 6 chars minimum.'),
	confirmPassword: Yup.string()
		.required()
		.oneOf([Yup.ref("password"), null], "Passwords must match")
});


const Register = () => {

	const dispatch = useDispatch();
	const history = useHistory();
	const location = useLocation();

	const {user, error} = useAuth();
	const [showModal, setShowModal] = useState(false);

	useEffect(() => {
		if (user) {
			const {from} = location.state || {from: {pathname: "/todos"}};
			history.replace(from);
		}
	}, [user, history, location.state])

	return (
		<Paper>
			<Formik
				initialValues={initValues}
				onSubmit={(values) => {
					dispatch(register(values))
						.then(response => {
							AuthorizationManager.setToken(response.payload.data.access_token);
							AuthorizationManager.setUser(values.rememberMe ? user.payload.data : null);
							history.push('/todos');
						})
						.catch(() => setShowModal(true));
				}}
				validationSchema={signUpSchema}
			>{({isSubmitting, dirty, isValid}) => (

				<Form>
					<Grid
						container
						direction="column"
						alignItems="center"
						spacing={1}
					>
						<Grid item>
							Sign Up
						</Grid>
						<TextField
							label="name"
							variant="outlined"
							name="username"
							type="input"
						/>
						<TextField
							label="email"
							variant="outlined"
							name="email"
							type="input"
						/>
						<TextField
							label="password"
							variant="outlined"
							name="password"
							type="password"
						/>
						<TextField
							label="confirm password"
							variant="outlined"
							name="confirmPassword"
							type="password"
						/>
						<Grid item>

						</Grid>
						<Grid item>
							<Button
								variant="contained"
								disabled={isSubmitting || !dirty || !isValid}
								type="submit"
							>
								Submit
							</Button>
						</Grid>
						<Grid item>
							<NavLink to="/login">
								You already have an account?
							</NavLink>
						</Grid>
					</Grid>
				</Form>
			)}
			</Formik>
			<Alert
				duration={3000}
				type='error'
				message={error?.response?.data?.error || error?.message}
				open={showModal}
				onClose={() => setShowModal(false)}
			/>
		</Paper>
	);
}

export default Register;
