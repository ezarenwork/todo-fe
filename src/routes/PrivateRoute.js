import React from 'react';
import {Redirect, Route} from "react-router";
import {useAuth} from "../store/auth/auth-selector";

const PrivateRoute = ({component: Component, ...rest}) => {
    const {user} = useAuth();
    return (
        <Route {...rest} render={props => (
            user
                ? <Component {...props}/>
                : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
        )}/>
    );
};

export default PrivateRoute;
