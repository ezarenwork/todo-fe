import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import AppRouter from './AppRouter';
import {getUser} from "./store/auth/auth-actions";
import {AuthorizationManager} from "./utils/authorization-manager";
import {useAuth} from "./store/auth/auth-selector";
import Loader from "./components/common/Loader";


const App = () => {
    const dispatch = useDispatch();
    const {isLoading} = useAuth();

    useEffect(() => {
        const user = AuthorizationManager.getUser();
        if (user) {
            dispatch(getUser(user));
        }
    }, [dispatch]);

    return (
        isLoading
            ? <Loader
                type="spinningBubbles"
                position="absolute"
                left="40%"
                top="40%"
            />
            : <AppRouter/>
    )
};


export default App;
