import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import logger from 'redux-logger'
import axios from 'axios';
import createSagaMiddleware from 'redux-saga';
import {
    createRequestInstance,
    requestsPromiseMiddleware,
    watchRequests
} from 'redux-saga-requests';
import {createDriver} from "redux-saga-requests-axios";
import todo from "./todo/todo-reducer";
import auth from "./auth/auth-reducer"
import {AuthorizationManager} from "../utils/authorization-manager";


axios.defaults.baseURL = 'http://127.0.0.1:8000/api';
axios.defaults.headers.common['Authorization'] = 'Bearer ' + AuthorizationManager.getToken();

function* rootSaga() {
    yield createRequestInstance({driver: createDriver(axios)});
    yield watchRequests();
}

const reducers = combineReducers({
    todo,
    auth
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
    thunk,
    requestsPromiseMiddleware({auto: true}),
    sagaMiddleware
];

if (process.env.NODE_ENV === `development`) {
    middlewares.push(logger);
}

export const store = createStore(
    reducers,
    applyMiddleware(...middlewares)
);

sagaMiddleware.run(rootSaga);


