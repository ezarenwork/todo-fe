import {requestActionCreator, POST} from "../../utils/request-helper";
import {success} from "redux-saga-requests";

export const LOGIN = 'LOGIN';
export const login = (userData) => requestActionCreator(LOGIN, 'account/login', POST, userData);

export const REGISTER = 'REGISTER';
export const register = (userData) => requestActionCreator(REGISTER, 'account/register', POST, userData);

export const LOGOUT = 'LOGOUT';
export const logout = () => requestActionCreator(LOGOUT, 'account/logout', POST);

export const FETCH_USER = 'FETCH_USER';
// export const fetchUser = () => requestActionCreator(FETCH_USER, '/account', POST);

export const GET_USER = 'GET_USER';
export const getUser = user => ({type: success(GET_USER), payload: {data: user}});



