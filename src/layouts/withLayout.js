import React from 'react';
import {Container} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";


import Header from "../components/Header";

const HeaderWrapper = () => {
    return (
        <header>
            <Header/>
        </header>
    )
};

const ContentWrapper = ({Component}) => {
    return (
        <section>
            <Component/>
        </section>
    )
};

const withLayout = (Component) => {

    return (props) => {
        return (
            <Container>
                    <Grid
                        container
                        direction="column"
                        alignItems="stretch"
                        justify="center"
                        spacing={5}
                    >
                        <Grid item>
                            <HeaderWrapper/>
                        </Grid>
                        <Grid item>
                            <ContentWrapper Component={Component}/>
                        </Grid>
                        <Grid item>
                            <footer>
                            </footer>
                        </Grid>
                    </Grid>
            </Container>
        )
    }
};

export default withLayout;
